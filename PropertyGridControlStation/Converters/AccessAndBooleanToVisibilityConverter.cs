﻿using PropertyGridControlStation.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace PropertyGridControlStation.Converters
{
    public class AccessAndBooleanToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            AccessTypes acsess = (AccessTypes)values[0];
            bool exist = (bool)values[1];

            if (acsess == AccessTypes.User || exist == false)
                return Visibility.Collapsed;
            else return Visibility.Visible;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
