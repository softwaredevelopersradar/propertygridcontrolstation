﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridControlStation.Model
{
    public class LocationInfo : AbstractModelWithCoordTranslation, IModelMethods<LocationInfo>
    {
        public bool EqualTo(LocationInfo model)
        {
            return Latitude == model.Latitude
                   && Longitude == model.Longitude
                   && LatitudeStr.Equals(model.LatitudeStr)
                   && LongitudeStr.Equals(model.LongitudeStr)
                   && ViewCoordField.Equals(ViewCoordField);
        }

        public LocationInfo Clone()
        {
            return new LocationInfo
            {
                Latitude = Latitude,
                Longitude = Longitude,
                LatitudeStr = (string)LatitudeStr.Clone(),
                LongitudeStr = (string)LongitudeStr.Clone(),
                ViewCoordField = ViewCoordField
            };
        }

        public void Update(LocationInfo model)
        {
            Longitude = model.Longitude;
            Latitude = model.Latitude;
            LongitudeStr = model.LongitudeStr;
            LatitudeStr = model.LatitudeStr;
            ViewCoordField = model.ViewCoordField;
        }
    }
}
