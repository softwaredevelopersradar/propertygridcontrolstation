﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace PropertyGridControlStation.Model
{
    public class BaseControlStationModel : IModelMethods<BaseControlStationModel>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };

        public BaseControlStationModel()
        {
            LocationInfo = new LocationInfo();
            Common = new Common();

            Common.PropertyChanged += PropertyChanged;
            LocationInfo.PropertyChanged += PropertyChanged;
        }

        protected void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(LocationInfo))]
        [DisplayName(" ")]
        public LocationInfo LocationInfo { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }


        public BaseControlStationModel Clone()
        {
            return new BaseControlStationModel
            {
                Common = Common.Clone(),
                LocationInfo = LocationInfo.Clone()
            };
        }

        public bool EqualTo(BaseControlStationModel model)
        {
            return Common == model.Common
                   && LocationInfo == model.LocationInfo;
        }

        public void Update(BaseControlStationModel model)
        {
            Common.Update(model.Common);
            LocationInfo.Update(model.LocationInfo);
        }

    }
}
