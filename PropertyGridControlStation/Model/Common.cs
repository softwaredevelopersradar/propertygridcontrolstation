﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using System.Xml.Linq;
using PropertyGridControlStation.Ext;

namespace PropertyGridControlStation.Model
{
    public class Common : AbstractBaseProperties<Common>
    {
        private string _note = "";
        private string _name = "";
        private string _displayName = "";
        private bool _isStationNowInUse = false;
        private bool _visibilityIsStationNowInUseField = true;

        [PropertyOrder(1)]
        public string Note
        {
            get => _note;
            set
            {
                if (_note == value) return;
                _note = value;
                OnPropertyChanged();
            }
        }

        [PropertyOrder(0)]
        public string DisplayName
        {
            get => _displayName;
            set
            {
                if (_displayName == value) return;
                _displayName = value;
                OnPropertyChanged();
            }
        }


        [Browsable(false)]
        public string Name
        {
            get => _name;
            set
            {
                if (_name == value) return;
                _name = value;
                OnPropertyChanged();
            }
        }


        [PropertyOrder(2)]
        public bool IsStationNowInUse
        {
            get => _isStationNowInUse;
            set
            {
                if (_isStationNowInUse == value) return;
                _isStationNowInUse = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool VisibilityIsStationNowInUseField
        {
            get => _visibilityIsStationNowInUseField;
            set
            {
                if (_visibilityIsStationNowInUseField == value) return;
                _visibilityIsStationNowInUseField = value;
                OnPropertyChanged();
            }
        }

        public override bool EqualTo(Common model)
        {
            return Note == model.Note
                  && Name == model.Name
                  && DisplayName == model.DisplayName
                  && IsStationNowInUse == model.IsStationNowInUse
                  && VisibilityIsStationNowInUseField == model.VisibilityIsStationNowInUseField;
        }

        public override Common Clone()
        {
            return new Common
            {
                Name = Name,
                Note = Note,
                DisplayName = DisplayName,
                IsStationNowInUse = IsStationNowInUse,
                VisibilityIsStationNowInUseField = VisibilityIsStationNowInUseField,
            };
        }

        public override void Update(Common model)
        {
            Name = model.Name;
            Note = model.Note;
            DisplayName = model.DisplayName;
            IsStationNowInUse = model.IsStationNowInUse;
            VisibilityIsStationNowInUseField = model.VisibilityIsStationNowInUseField;
        }
    }
}
