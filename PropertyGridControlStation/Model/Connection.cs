﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using PropertyGridControlStation.Ext;

namespace PropertyGridControlStation.Model
{
    public class Connection : AbstractBaseProperties<Connection>
    {
        private int portGsm = 1234;
        private int portEthernet = 9114;
        private string ipAddressGsm = "86.107.139.190";
        private string ipAddressRadio = "37.17.12.133";
        private string ipAddressEthernet = "192.168.10.102";
        private bool isRadioExist = true;
        private bool isEthernetExist = true;
        private bool isGSMExist = true;


        [NotifyParentProperty(true)]
        [PropertyOrder(1)]
        [Range(0, 100000)]
        public int PortGSM
        {
            get => portGsm;
            set
            {
                if (portGsm == value) return;
                portGsm = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [PropertyOrder(1)]
        [Range(0, 100000)]
        public int PortEthernet
        {
            get => portEthernet;
            set
            {
                if (portEthernet == value) return;
                portEthernet = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Required]
        [PropertyOrder(0)]
        [RegularExpression(@"^(?!127\.0\.0\.1)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IPAddressGSM
        {
            get => ipAddressGsm;
            set
            {
                if (ipAddressGsm == value) return;
                ipAddressGsm = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Required]
        [PropertyOrder(0)]
        [RegularExpression(@"^(?!127\.0\.0\.1)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IPAddressRadio
        {
            get => ipAddressRadio;
            set
            {
                if (ipAddressRadio == value) return;
                ipAddressRadio = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [PropertyOrder(0)]
        [RegularExpression(@"^(?!127\.0\.0\.1)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IPAddressEthernet
        {
            get => ipAddressEthernet;
            set
            {
                if (ipAddressEthernet == value) return;
                ipAddressEthernet = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsRadioExist
        {
            get => isRadioExist;
            set
            {
                if (isRadioExist == value) return;
                isRadioExist = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsEthernetExist
        {
            get => isEthernetExist;
            set
            {
                if (isEthernetExist == value) return;
                isEthernetExist = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsGSMExist
        {
            get => isGSMExist;
            set
            {
                if (isGSMExist == value) return;
                isGSMExist = value;
                OnPropertyChanged();
            }
        }

        public override Connection Clone()
        {
            return new Connection
            {
                PortGSM = PortGSM,
                PortEthernet = PortEthernet,
                IPAddressGSM = IPAddressGSM,
                IPAddressRadio = IPAddressRadio,
                IPAddressEthernet = IPAddressEthernet,
                IsRadioExist = IsRadioExist,
                IsGSMExist = IsGSMExist,
                IsEthernetExist = IsEthernetExist
            };
        }

        public override bool EqualTo(Connection model)
        {
            return PortGSM == model.PortGSM
                   && PortEthernet == model.PortEthernet
                   && IPAddressGSM == model.IPAddressGSM
                   && IPAddressRadio == model.IPAddressRadio
                   && IPAddressEthernet == model.IPAddressEthernet
                   && IsEthernetExist == model.IsEthernetExist
                   && IsGSMExist == model.IsGSMExist
                   && IsRadioExist == model.IsRadioExist;
        }

        public override void Update(Connection model)
        {
            PortGSM = model.PortGSM;
            PortEthernet = model.PortEthernet;
            IPAddressGSM = model.IPAddressGSM;
            IPAddressRadio = model.IPAddressRadio;
            IPAddressEthernet = model.IPAddressEthernet;
            IsEthernetExist = model.IsEthernetExist;
            IsGSMExist = model.IsGSMExist;
            IsRadioExist = model.IsRadioExist;
        }
    }
}
