﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using YamlDotNet.Serialization;

namespace PropertyGridControlStation.Model
{
    [CategoryOrder(nameof(Common), 1)]
    [CategoryOrder(nameof(LocationInfo), 2)]
    [CategoryOrder(nameof(Connection), 3)]

    public class ControlStationModel : BaseControlStationModel, IModelMethods<BaseControlStationModel>
    {
        public ControlStationModel() : base()
        {
            Connection = new Connection();

            Connection.PropertyChanged += PropertyChanged;
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Connection))]
        [DisplayName(" ")]
        public Connection Connection { get; set; }

        public ControlStationModel Clone()
        {
            return new ControlStationModel
            {
                Common = Common.Clone(),
                Connection = Connection.Clone(),
                LocationInfo = LocationInfo.Clone()
            };
        }

        public bool EqualTo(ControlStationModel model)
        {
            return Common == model.Common
                   && Connection == model.Connection
                   && LocationInfo == model.LocationInfo;
        }

        public void Update(ControlStationModel model)
        {
            Common.Update(model.Common);
            Connection.Update(model.Connection);
            LocationInfo.Update(model.LocationInfo);
        }
    
    }
}
