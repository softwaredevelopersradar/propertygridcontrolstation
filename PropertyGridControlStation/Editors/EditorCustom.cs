﻿using PropertyGridControlStation.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;


namespace PropertyGridControlStation.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(ControlStationModel.Common), "CommonEditorKey" },
            { nameof(ControlStationModel.Connection), "ConnectionEditorKey" },
            { nameof(ControlStationModel.LocationInfo), "CoordinatesEditorKey" }
        };

       
        public EditorCustom(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PropertyGridControlStation;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }

        public EditorCustom(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PropertyGridControlStation;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }

    }
}
