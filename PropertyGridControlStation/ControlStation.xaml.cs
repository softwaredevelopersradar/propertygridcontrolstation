﻿using PropertyGridControlStation.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using PropertyGridControlStation.Editors;
using AccessTypes = PropertyGridControlStation.Model.AccessTypes;

namespace PropertyGridControlStation
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class ControlStation : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<ControlStationModel> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<ControlStationModel> OnApplyButtonClick = (sender, obj) => { };
        public event EventHandler<ControlStationModel> OnDeleteButtonClick = (sender, obj) => { };
        public event EventHandler<string> OnIpAddressChanged = (sender, ipAddress) => { };
        public event EventHandler<ControlStationModel> OnExitButtonClick = (sender, obj) => { };
        #endregion

        #region Properties

        #region Dependency properties

        public static readonly DependencyProperty DependencyAccess =
           DependencyProperty.Register("Access", typeof(AccessTypes), typeof(ControlStation), new PropertyMetadata(AccessTypes.User, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        public static readonly DependencyProperty DependencyLanguage =
            DependencyProperty.Register("Language", typeof(Language), typeof(ControlStation), new PropertyMetadata(Language.EN, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityIsEthernetExist =
           DependencyProperty.Register("VisibilityEthernet", typeof(bool), typeof(ControlStation), new PropertyMetadata(true, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityIsRadioExist =
            DependencyProperty.Register("VisibilityRadio", typeof(bool), typeof(ControlStation), new PropertyMetadata(true, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityIsGSMExist =
            DependencyProperty.Register("VisibilityGSM", typeof(bool), typeof(ControlStation), new PropertyMetadata(true, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        public bool VisibilityEthernet
        {
            get => (bool)GetValue(dependencyVisibilityIsEthernetExist);
            set => SetValue(dependencyVisibilityIsEthernetExist, value);
        }

        public bool VisibilityRadio
        {
            get => (bool)GetValue(dependencyVisibilityIsRadioExist);
            set => SetValue(dependencyVisibilityIsRadioExist, value);
        }

        public bool VisibilityGSM
        {
            get => (bool)GetValue(dependencyVisibilityIsGSMExist);
            set => SetValue(dependencyVisibilityIsGSMExist, value);
        }

        public AccessTypes Access
        {
            get => (AccessTypes)GetValue(DependencyAccess);
            set => SetValue(DependencyAccess, value);
        }

        public new Language Language
        {
            get => (Language)GetValue(DependencyLanguage);
            set => SetValue(DependencyLanguage, value);
        }

        private static void OnDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ControlStation mapObjectPropGrid = d as ControlStation;
            mapObjectPropGrid.OnDependencyPropertyChanged(e);
        }

        private void OnDependencyPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(Language):
                    ChangeLanguage((Language)e.NewValue);
                    break;
                case nameof(Access):
                    if ((AccessTypes)e.NewValue == AccessTypes.Admin)
                        ChangeAdminPropertyVisibility(true);
                    else ChangeAdminPropertyVisibility(false);
                    break;
                case nameof(VisibilityEthernet):
                    Local.Connection.IsEthernetExist = (bool)e.NewValue;
                    break;
                case nameof(VisibilityRadio):
                    Local.Connection.IsRadioExist = (bool)e.NewValue;
                    break;
                case nameof(VisibilityGSM):
                    Local.Connection.IsGSMExist = (bool)e.NewValue;
                    break;

            }
        }

        #endregion






        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private ControlStationModel savedLocal;

        public ControlStationModel Local
        {
            get => (ControlStationModel)Resources["localProperties"];
            set
            {
                if ((ControlStationModel)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    return;
                }
                ((ControlStationModel)Resources["localProperties"]).Update(value);
                savedLocal = value.Clone();
            }
        }
        #endregion


        #region Access

        private void ChangeAdminPropertyVisibility(bool browsable)
        {
            PropertyLocal.Properties[nameof(ControlStationModel.Connection)].IsBrowsable = browsable;
        }

        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Language newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryLocalNames();

        }
        private void SetDynamicResources(Language newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Model.Language.EN:
                        dict.Source = new Uri("/PropertyGridControlStation;component/Language/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Model.Language.RU:
                        dict.Source = new Uri("/PropertyGridControlStation;component/Language/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Model.Language.AZ:
                        dict.Source = new Uri("/PropertyGridControlStation;component/Language/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/PropertyGridControlStation;component/Language/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
                //TODO
            }
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }


        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher" || property.Name == "BRD"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            //  Console.WriteLine(property.Name+ "  " + subProperty.Name);
                            continue;
                        }

                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            funcValidation(Local);

        }

        #endregion

        public ControlStation()
        {
            InitializeComponent();
            InitLocalProperties();
            ChangeLanguage(Language);
            Validate();
        }

        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new EditorCustom(nameof(Local.LocationInfo), typeof(ControlStationModel)));
            PropertyLocal.Editors.Add(new EditorCustom(nameof(Local.Connection), typeof(ControlStationModel)));
            PropertyLocal.Editors.Add(new EditorCustom(nameof(Local.Common), typeof(ControlStationModel)));
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
            Local.Connection.IsEthernetExist = VisibilityEthernet;
            Local.Connection.IsGSMExist = VisibilityGSM;
            Local.Connection.IsRadioExist = VisibilityRadio;
            switch (e.PropertyName)
            {
                case nameof(Local.Connection.IPAddressGSM):
                    OnIpAddressChanged("GSM", Local.Connection.IPAddressGSM);
                    break;
                case nameof(Local.Connection.IPAddressEthernet):
                    OnIpAddressChanged("Ethernet", Local.Connection.IPAddressEthernet);
                    break;
                case nameof(Local.Connection.IPAddressRadio):
                    OnIpAddressChanged("Radio", Local.Connection.IPAddressRadio);
                    break;
                default:
                    break;
            }
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            OnApplyButtonClick(this, Local);
        }

        private void butDelete_Click(object sender, RoutedEventArgs e)
        {
            OnDeleteButtonClick(this, Local);
        }

        private void butExit_Click(object sender, RoutedEventArgs e)
        {
            OnExitButtonClick(sender, Local);
        }
        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                this.OnApplyButtonClick(this, Local);
            else if (e.Key == Key.Escape)
                this.OnDeleteButtonClick(this, Local);
        }

        private void Grid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta / 3);
        }
    }
}
